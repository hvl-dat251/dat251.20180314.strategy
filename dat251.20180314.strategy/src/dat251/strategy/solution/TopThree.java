package dat251.strategy.solution;

public class TopThree {
	
	//SortingStrategy sorter = new BubbleSortSortingStrategy();
	SortingStrategy sorter = new SelcetionSortSortingStrategy();
	
	int[] scores;
	
	public TopThree(int[] scores) {
		this.scores = scores;
	}

	public int getGoldScore() {
		sorter.sort(scores);
		return scores[scores.length-1];
	}
	
	public int getSilverScore() {
		sorter.sort(scores);
		return scores[scores.length-2];
	}
	
	public int getBronzeScore() {
		sorter.sort(scores);
		return scores[scores.length-3];
	}
}
