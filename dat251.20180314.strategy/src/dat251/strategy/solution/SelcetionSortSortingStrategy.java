package dat251.strategy.solution;

public class SelcetionSortSortingStrategy implements SortingStrategy {

	public void sort(int[] values) {
		for (int nSorted = 0; nSorted < values.length; nSorted++) {

			int indexMin = nSorted;
			int min = values[nSorted];

			for (int i = nSorted; i < values.length; i++) {

				if (values[i] < min) {
					min = values[i];
					indexMin = i;
				}
			}

			values[indexMin] = values[nSorted];
			values[nSorted] = min;
		}
	}

}
