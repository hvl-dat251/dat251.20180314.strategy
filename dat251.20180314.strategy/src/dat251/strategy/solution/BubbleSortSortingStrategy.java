package dat251.strategy.solution;

public class BubbleSortSortingStrategy implements SortingStrategy {

	public void sort(int[] values) {

		for (int nSorted = 0; nSorted < values.length; nSorted++) {
			for (int i = 0; i < (values.length - nSorted - 1); i++) {
				if (values[i] > values[i + 1]) {
					int temp = values[i];
					values[i] = values[i + 1];
					values[i + 1] = temp;
				}
			}
		}
	}
}
