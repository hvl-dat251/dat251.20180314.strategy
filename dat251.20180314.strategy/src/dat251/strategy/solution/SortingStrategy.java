package dat251.strategy.solution;

public interface SortingStrategy {
	
	public void sort(int[] values);

}
